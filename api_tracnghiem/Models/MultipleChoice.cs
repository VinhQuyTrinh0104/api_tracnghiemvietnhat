﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace api_tracnghiem.Models
{
    public class MultipleChoice
    {
        public int QuestionId { get; set; }
        public string QuestionContent { get; set; }
        public int AnswerId_A { get; set; }
        public string AnswerContent_A { get; set; }
        public int AnswerId_B { get; set; }
        public string AnswerContent_B { get; set; }
        public int AnswerId_C { get; set; }
        public string AnswerContent_C { get; set; }
        public int AnswerId_D { get; set; }
        public string AnswerContent_D { get; set; }
        public int AnswerId_True { get; set; }
        //public string Time
    }
}