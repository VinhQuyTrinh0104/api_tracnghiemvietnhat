﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace api_tracnghiem.Models
{
	public class StringArrayShuffler
	{
		private Random random;

		public StringArrayShuffler()
		{
			random = new Random();
		}

		public int[] ShuffleArray(int[] inputArray)
		{
			int n = inputArray.Length;
			int[] shuffledArray = new int[n];

			// Sao chép các phần tử từ mảng gốc vào mảng đã random đảo
			Array.Copy(inputArray, shuffledArray, n);

			// Sử dụng thuật toán Fisher-Yates để random đảo các phần tử
			for (int i = n - 1; i > 0; i--)
			{
				int j = random.Next(0, i + 1);
				int temp = shuffledArray[i];
				shuffledArray[i] = shuffledArray[j];
				shuffledArray[j] = temp;
			}

			return shuffledArray;
		}
	}
}