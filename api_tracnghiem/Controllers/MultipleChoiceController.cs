﻿using api_tracnghiem.App_Start;
using api_tracnghiem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api_tracnghiem.Controllers
{
	public class MultipleChoiceController : ApiController
	{
		List<MultipleChoice> multipleChoices = new List<MultipleChoice>();
		dbcsdlDataContext db = new dbcsdlDataContext();
		[HttpGet]
		[Route("multiple-choice-{id}")]
		public IHttpActionResult GetMultipleChoice(int id)
		{

			var getData = from lt in db.tbTracNghiem_BaiLuyenTaps
						  join t in db.tbTracNghiem_Tests on lt.luyentap_id equals t.luyentap_id
						  where t.test_id == id
						  select new
						  {
							  lt.luyentap_danhsachbai,
							  t.test_soluongcauhoi,
							  lt.luyentap_tilecauhoi,
							  t.test_thoigianlambai,
						  };
			//txtThoiGian.Value = getData.FirstOrDefault().test_thoigianlambai; // thời gian làm bài 
			int soluongcau = Convert.ToInt32(getData.FirstOrDefault().test_soluongcauhoi);
			string[] arrListStr = getData.FirstOrDefault().luyentap_danhsachbai.Split(',');
			string[] arrTiLeCauHoi = getData.FirstOrDefault().luyentap_tilecauhoi.Split(';'); //40/40/30
			for (var i = 0; i < arrTiLeCauHoi.Length; i++)
			{
				arrTiLeCauHoi[i] = Convert.ToString(Convert.ToInt32(arrTiLeCauHoi[i]) * soluongcau / 100); //tổng số câu của từng bài => 10 câu hỏi của 1 bài
			}

			for (var j = 0; j < arrListStr.Length; j++)//35:37
			{
				List<int> arrIdQuestion = new List<int>();
				var getIdQuestion = (from qs in db.tbTracNghiem_Questions
									 where qs.lesson_id == Convert.ToInt32(arrListStr[j]) //35:37 : 50
									 select new
									 {
										 qs.question_id,
									 });//.Take(arrListStr.Length == 1 ? Convert.ToInt32(arrTiLeCauHoi[j]) + 10 : Convert.ToInt32(arrTiLeCauHoi[j])); //.Take(arrListStr.Length == 1 ? 30 : 20)

				foreach (var item in getIdQuestion)
				{
					arrIdQuestion.Add(item.question_id);
				}

				List<int> distinctNumbers = GetRandomDistinctNumbers(arrIdQuestion, Convert.ToInt32(arrTiLeCauHoi[j]));

				for (int i = 0; i < distinctNumbers.Count(); i++)
				{
					var getAnswer = from qs in db.tbTracNghiem_Questions
									join aw in db.tbTracNghiem_Answers on qs.question_id equals aw.question_id
									where qs.question_id == distinctNumbers[i]
									select new
									{
										aw.answer_id,
									};
					var getAnswerTrue = (from qs in db.tbTracNghiem_Questions
										 join aw in db.tbTracNghiem_Answers on qs.question_id equals aw.question_id
										 where qs.question_id == distinctNumbers[i]
										 && aw.answer_true == true
										 select aw).FirstOrDefault().answer_id;

					int[] arrAnswer = new int[4];
					int temp = 0;
					foreach (var item in getAnswer)
					{
						arrAnswer[temp] = item.answer_id;
						temp++;
					}
					StringArrayShuffler shuffler = new StringArrayShuffler();
					int[] shuffledArray = shuffler.ShuffleArray(arrAnswer);
					var question_content = (from qs in db.tbTracNghiem_Questions
											where qs.question_id == distinctNumbers[i]
											select new
											{
												qs.question_content,
												qs.question_giaithich,
											}).FirstOrDefault();
					multipleChoices.Add(new MultipleChoice
					{
						QuestionId = distinctNumbers[i],
						QuestionContent = question_content.question_content,
						AnswerContent_A = db.tbTracNghiem_Answers.Where(x => x.answer_id == shuffledArray[0]).Select(d => d.answer_content).FirstOrDefault(),
						AnswerContent_B = db.tbTracNghiem_Answers.Where(x => x.answer_id == shuffledArray[1]).Select(d => d.answer_content).FirstOrDefault(),
						AnswerContent_C = db.tbTracNghiem_Answers.Where(x => x.answer_id == shuffledArray[2]).Select(d => d.answer_content).FirstOrDefault(),
						AnswerContent_D = db.tbTracNghiem_Answers.Where(x => x.answer_id == shuffledArray[3]).Select(d => d.answer_content).FirstOrDefault(),
						AnswerId_A = shuffledArray[0],
						AnswerId_B = shuffledArray[1],
						AnswerId_C = shuffledArray[2],
						AnswerId_D = shuffledArray[3],
						AnswerId_True = getAnswerTrue,
					});
				}
			}
			return Ok(multipleChoices);
		}

		private List<int> GetRandomDistinctNumbers(List<int> inputList, int count)
		{
			Random random = new Random();
			List<int> distinctList = inputList.Distinct().ToList();
			List<int> result = new List<int>();
			if (inputList.Count() > count)
			{
				while (result.Count < count)
				{
					int randomIndex = random.Next(0, distinctList.Count);
					int number = distinctList[randomIndex];
					result.Add(number);
					distinctList.RemoveAt(randomIndex);
				}
			}
			else
			{
				result = distinctList;
			}
			return result;
		}
	}
}
